# go_real_md

### 介绍

golang 接收 CTP 实时行情

### 软件架构

1. 采用 goctp 接口订阅行情
2. 接收后合成分钟数据落入 redis
3. 以 md.{instrumentid}发布分钟数据，应用端可订阅后接收分钟数据。
4. 收盘后分钟数据保存至 postgres 数据库中
5. 前端
   1. 数据库查询
   2. sse 推送实时数据(bar tick)
   3. 订阅/退订

### 分钟处理

- 只处理处于可交易状态的品种（会过滤掉开/收时的 tick）
- 处理 actionDay
  - tradingday 前一**交易日**为 actionDay
  - actionDay 下一**自然日**为 actionDayNight
  - hour>=17 取 actionDay
  - hour<=3 取 actionDayNight
  - hour 其他 取 tradingDay
- 分钟 Volume
  - preVol 前一分钟最后 tick 的 Volume
  - 当前分钟的 Volume = tick.Volume-preVol

### 使用说明

#### 环境变量

| 变量       | 默认值                                            | 说明                                            |
| ---------- | ------------------------------------------------- | ----------------------------------------------- |
| tradeFront | tcp://180.168.146.187:10130                       | ctp 交易前置                                    |
| quoteFront | tcp://180.168.146.187:10131                       | ctp 行情前置                                    |
| loginInfo  | 9999/008107/1/simnow_client_test/0000000000000000 | 登录配置格式 broker/investor/pwd/appid/authcode |
| redisAddr  | 127.0.0.1:6379                                    | redis 库配置 host:port                          |
| pgMin      | 127.0.0.1:5432                                    | 分钟 pg 库配置                                  |

### 生成镜像

```bash
make build
# 推送镜像
# - 修改 Makefile L6 REGISTRY 的值为自己的镜像库地址
make docker
```

#### docker-compose.yml

```yaml
version: '3.7'
services:
  # 启动: docker-compose --compatibility up -d
  realmd:
    image: haifengat/go_real_md:20220730
    container_name: realmd
    restart: on-failure:3
    environment:
      - 'TZ=Asia/Shanghai'
      - 'redisAddr=redis_real:6379'
      - 'tradeFront=tcp://180.168.146.187:10101'
      - 'quoteFront=tcp://180.168.146.187:10111'
      - 'loginInfo=008105/1/9999/simnow_client_test/0000000000000000'
      - 'pgMin=postgresql://postgres:12345@pg_real:5432/postgres?sslmode=disable'
      # 订阅合约列表 p1,p2... 默认订阅所有合约
      # - "products="
    ports:
      - 8080:8080
    deploy:
      resources:
        limits:
          cpus: '1'
          memory: 2G
        reservations:
          memory: 200M
    depends_on:
      - redis_real
      - pg_real
  # 遇到the database system is starting up错误, 配置数据文件下的postgres.conf,hot_standby=on
  pg_real:
    image: postgres
    container_name: pg_real
    restart: on-failure:3
    environment:
      TZ: 'Asia/Shanghai'
      POSTGRES_PASSWORD: '12345'
    volumes:
      - ./pg_data:/var/lib/postgresql/data
  redis_real:
    image: redis
    container_name: redis_real
    restart: on-failure:3
    environment:
      - TZ=Asia/Shanghai
```

#### 内容查看

```sh
# 进入 docker redis cli
docker exec -it ${redis_docker} redis-cli
# 合约列表
> KEYS *
# 查看存储的行情
> LRANGE ${合约} 0 -1
# 查看存储的交易日
> HGET tradingday curday
```

### postgres

#### 导入导出

```sql
# 数据导出
docker exec -it pg容器 pg_dump -U postgres |gzip > ./`date +%Y%m%d`.sql.gz
# 数据导入
gzip -dc ./`date +%Y%m%d`.sql.gz | docker exec -i pg容器 psql -U postgres -d 数据库
```

### 本地部署

```sh
# 查看日志
docker logs -f go_real_md
```

## 附

### 行情订阅后收不到 ontick 响应

原因：交易所状态处理问题
处理：已修复

### 接口断开重连，收不到 login 响应

原因：猜测为匿名函数被回收
解决：实际函数替代匿名函数

### 收盘时间的 tick 仍被处理

双 tick 仍无法避免，即 15:00:00 时收到 2 两个 tick。例：y2105 20201214
解决：3tick

### concurrent map read and map write

原因是 mapMin 变量用 map[string]interface{}保存分钟数据，在 lastInstMin 读取时冲突
解决：改为 Bar{}

### pq: duplicate key value violates unique constraint "future_min_datetime_instrument

ticks 改用 bar.ticks 处理
分钟是否更新用 > 判断，防止小于当前分钟的数据生成
