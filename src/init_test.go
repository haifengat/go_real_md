package src

import "os"

func init() {
	os.Chdir("../")
	os.Setenv("redisAddr", "47.116.126.3:6379")
	os.Setenv("pgMin", "postgresql://postgres:12345@localhost:5432/postgres?sslmode=disable")
	os.Setenv("tradeFront", "tcp://180.168.146.187:10301")
	os.Setenv("quoteFront", "tcp://180.168.146.187:10311")
	os.Setenv("loginInfo", "8888/0042000021/SimNow~8888/0042_SimNowTest_001/92Z6EFTXYXMRKVF5")
	initEnv()
}
