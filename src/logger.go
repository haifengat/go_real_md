package src

import (
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

func init() {
	// 日志中打印时间
	logrus.SetFormatter(&logrus.TextFormatter{TimestampFormat: time.DateTime, FullTimestamp: true})

	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetOutput(os.Stdout)
	// logrus.SetReportCaller(true) // 显示函数和行号
}
