package src

import (
	"context"
	"os"
	"strings"
	"time"

	"gitee.com/chunanyong/zorm"
	zd "gitee.com/haifengat/zorm-dm/v2"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

var (
	tradeFront, quoteFront, loginInfo, brokerID, investorID, password, appID, authCode string
	rdb                                                                                *redis.Client   // redis 连接
	ctxRedis, ctxDAO                                                                   context.Context // redis 上下文
)

// NewRealMd realmd 实例
func initEnv() {
	// 不打印SQL
	zorm.FuncPrintSQL = func(ctx context.Context, sqlstr string, args []interface{}, execSQLMillis int64) {}

	if ctxDAO != nil {
		return
	}
	// 环境变量读取,赋值
	var tmp string
	if tmp = os.Getenv("tradeFront"); tmp == "" {
		logrus.Fatal("未配置环境变量: tradeFront")
	}
	tradeFront = tmp
	if tmp = os.Getenv("quoteFront"); tmp == "" {
		logrus.Fatal("未配置环境变量: quoteFront")
	}
	quoteFront = tmp
	if tmp = os.Getenv("loginInfo"); tmp == "" {
		logrus.Fatal("未配置环境变量: loginInfo")
	}
	loginInfo = tmp

	fs := strings.Split(loginInfo, "/")
	brokerID, investorID, password, appID, authCode = fs[0], fs[1], fs[2], fs[3], fs[4]
	if !strings.HasPrefix(tradeFront, "tcp://") {
		tradeFront = "tcp://" + tradeFront
	}
	if !strings.HasPrefix(quoteFront, "tcp://") {
		quoteFront = "tcp://" + quoteFront
	}

	var redisAddr = ""
	if tmp = os.Getenv("redisAddr"); tmp == "" {
		logrus.Error("未配置环境变量: redisAddr, 不能订阅实时行情.")
	} else {
		redisAddr = tmp

		logrus.Info("redis: ", redisAddr)
		rdb = redis.NewClient(&redis.Options{
			Addr:         redisAddr,
			Password:     "",  // no password set
			DB:           0,   // use default DB
			PoolSize:     100, // 连接池最大socket连接数，默认为4倍CPU数， 4 * runtime.NumCPU
			MinIdleConns: 10,  //在启动阶段创建指定数量的Idle连接，并长期维持idle状态的连接数不少于指定数量；
			//超时
			DialTimeout:  5 * time.Second, //连接建立超时时间，默认5秒。
			ReadTimeout:  3 * time.Second, //读超时，默认3秒， -1表示取消读超时
			WriteTimeout: 3 * time.Second, //写超时，默认等于读超时
			PoolTimeout:  3 * time.Second, //当所有连接都处在繁忙状态时，客户端等待可用连接的最大等待时长，默认为读超时+1秒
		})
		ctxRedis = context.Background()
		ctx, cancelFunc := context.WithTimeout(ctxRedis, time.Second*5)
		defer cancelFunc()
		pong, err := rdb.Ping(ctx).Result()
		if err != nil {
			logrus.Fatal(pong, err)
		}
	}

	pgMin := os.Getenv("pgMin")
	if pgMin == "" {
		logrus.Warn("未配置 pgMin, 收盘后将不入库！")
	} else {
		logrus.Info("postgres :", pgMin)
		var schemaName = "future" // 模式
		var err error
		ctxDAO, err = zd.InitDaoPostgres(pgMin, schemaName)
		if err != nil {
			logrus.Fatal("pgMin 配置错误:", err)
		}
		if err = zd.CreateSchema(ctxDAO); err == nil {
			if err = zd.CreateTable[Bar](ctxDAO); err == nil {
				if err = zd.CreateTable[InstrumentField](ctxDAO); err == nil {
					if err = zd.CreateTable[PositionField](ctxDAO); err == nil {
						if err = zd.CreateTable[TradeField](ctxDAO); err == nil {
							if err = zd.CreateTable[OrderField](ctxDAO); err == nil {
								err = zd.CreateTable[AccountField](ctxDAO)
							}
						}
					}
				}
			}
		}
		if err != nil {
			logrus.Fatal(err)
		}
	}
}
