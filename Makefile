.DEFAULT_GOAL := help

# 获取项目名称（从远程仓库URL中提取）
PROJECT_NAME := $(shell basename -s .git $$(git config --get remote.origin.url))
# 获取当前分支名称
BRANCH_NAME := $(shell git rev-parse --abbrev-ref HEAD)

# 判断分支名是否为 master 并设置最终的 PROJECT_NAME
	ifeq ($(BRANCH_NAME),master)
PROJECT_NAME := $(PROJECT_NAME)
	else
PROJECT_NAME := $(PROJECT_NAME)-$(BRANCH_NAME)
	endif
	
# 可用参数定义 CURDATE 的值
CURDATE?=$(shell date '+%Y%m%d')
REGISTRY=registry.cn-shanghai.aliyuncs.com/haifengat
AppName=${PROJECT_NAME}
$(shell rm -rf ./lib)
$(shell mkdir -p ./lib)
$(shell \cp ~/go/pkg/mod/gitee.com/haifengat/goctp/v2\@$(shell cat go.mod |grep goctp|sed 's:\s\+:\t:g'|cut -f3)/lib/*.so ./lib)
#================================= 镜像处理 =============================#
build: # 编译 打包镜像
	go build -o ${PROJECT_NAME} cmd/main.go
	@# 替换容器中的 appName 以解决 entrypoint 启动程序不能用变量的问题
	sed -i 's#ENTRYPOINT.*#ENTRYPOINT ["./${PROJECT_NAME}"]#g' Dockerfile
	docker build . -t ${REGISTRY}/${PROJECT_NAME}:${CURDATE} --no-cache --build-arg AppName=${CURDIRNAME}
	sed -i 's#ENTRYPOINT.*#ENTRYPOINT ["./$${AppName}"]#g' Dockerfile
	rm -rf ./lib
	rm -f ${PROJECT_NAME}
docker: build # 镜像推送
	docker push ${REGISTRY}/${PROJECT_NAME}:${CURDATE}
#=================================== 部署 =====================================#
local:
	docker rm -f ${PROJECT_NAME}
	docker run -d --name ${PROJECT_NAME} \
	--privileged \
	--restart=always \
	-p 8080:4000 \
	-e redisAddr=47.116.126.3:6379 \
	-e pgMin=postgresql://postgres:12345@host.docker.internal:5432/postgres?sslmode=disable \
	-e tradeFront=tcp://180.168.146.187:10301 \
	-e quoteFront=tcp://180.168.146.187:10311 \
	-e loginInfo=8888/0042000021/SimNow~8888/0042_SimNowTest_001/92Z6EFTXYXMRKVF5 \
	${REGISTRY}/${PROJECT_NAME}:${CURDATE}
	echo "$(shell date '+%Y/%m/%d %H:%M:%S') ${MAKECMDGOALS} ${REGISTRY}/${PROJECT_NAME}:${CURDATE}" >> deploy.log

.PHONY: build docker
.PHONY: help
help:
	@echo '镜像生成并推送:     make docker'
	@echo 'make -n 检查语法'
	@echo 'make xxxx CURDATE=yyyymmdd 指定日期(版本)'
