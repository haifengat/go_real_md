package main

import (
	"realmd/src"
	"time"

	"gitee.com/haifengat/gin-ex/middleware"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func main() {
	// 应用启动
	go src.Run724(false)
	r := gin.Default()
	r.Use(middleware.Cors)

	api := r.Group("/api")
	{
		src.Route(api)
	}
	time.Sleep(time.Second * 6)
	logrus.Info("服务启动成功, http://localhost:4000")
	r.Run(":4000")
}
