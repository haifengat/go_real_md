package main

import (
	"os"
	"testing"
)

func init() {
	os.Chdir("../")

	// 阿里
	// os.Setenv("redisAddr", "47.116.126.3:26379")
	// os.Setenv("pgMin", "postgresql://postgres:12345@47.116.126.3:25432/postgres?sslmode=disable")
	// 本地
	os.Setenv("redisAddr", "47.116.126.3:6379")
	os.Setenv("pgMin", "postgresql://postgres:12345@localhost:5432/postgres?sslmode=disable")
	// os.Setenv("tradeFront", "tcp://180.168.146.187:10130")
	// os.Setenv("quoteFront", "tcp://180.168.146.187:10131")
	// os.Setenv("loginInfo", "9999/008107/1/0042_SimNowTest_001/0000000000000000")
	os.Setenv("tradeFront", "tcp://180.168.146.187:10301")
	os.Setenv("quoteFront", "tcp://180.168.146.187:10311")
	os.Setenv("loginInfo", "8888/0042000021/SimNow~8888/0042_SimNowTest_001/92Z6EFTXYXMRKVF5")
}
func TestMain(t *testing.T) {
	main()
}
