# 先编码再做镜像(要用centos基础镜像)
# go build -o realmd
# docker build -t haifengat/go_real_md:`date +%Y%m%d` .
# FROM centos:centos8.2.2004 AS final
# FROM ubuntu:20.04
# RUN apt update; \
#   apt install tzdata; \
#   rm /etc/localtime; \
#   ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# FROM busybox:glibc
FROM registry.cn-shanghai.aliyuncs.com/haifengat/openeuler:24.03-lts
WORKDIR /app

COPY ./Shanghai /usr/share/zoneinfo/Asia/
RUN ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

ARG appName

COPY ./${AppName} ./
COPY ./lib ./

ENV LD_LIBRARY_PATH /app

ENTRYPOINT ["./${AppName}"]
